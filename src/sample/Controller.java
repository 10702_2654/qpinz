package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Path;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public Button BtnStart;
    public ProgressBar pbStart;
    public ImageView iv;

    public void onStart(ActionEvent actionEvent) {


        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final int max = 100;

                pbStart.setVisible(true);
                iv.setVisible(true);
                /*File source = new File("C:/Users/CSIE-E518/Pictures");
                File dest = new File("C:/Users/CSIE-E518/Desktop");
                Files.copy(source.toPath(), dest.toPath());*/

                for(int i=0;i<=max;i++){
                    updateProgress(i,max);
                    Thread.sleep(100);
                }
                return null;
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                pbStart.setVisible(false);
                iv.setVisible(false);
            }
        };

        pbStart.progressProperty().bind(task.progressProperty());
        new Thread(task).start();

        List<String> str = new ArrayList<>();

        for(int i=1;i<7;i++){
            str.add("/AMG/".concat(String.valueOf(i)).concat(".jpg"));
        }

        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        for(int i=0;i<5;i++){
            int j=i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(str.get(j)).toString()));
                        }
                    })
            );
        }

        timeline.play();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
